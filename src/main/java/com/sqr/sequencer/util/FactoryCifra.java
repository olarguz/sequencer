package com.sqr.sequencer.util;

import com.sqr.sequencer.data.Cifra;
import com.sqr.sequencer.mapper.Transformable;
import com.sqr.sequencer.mapper.impl.Entero;

/**
 *
 * @author olmedo.arcila
 * @param <T>
 */
public class FactoryCifra<T extends Transformable> {

    private final int valMin;
    private final int valMax;
    private final T min;
    private final T max;

    public FactoryCifra(final int valMin, final int valMax, final T min, final T max) {
        this.valMin = valMin;
        this.valMax = valMax;
        this.min = min;
        this.max = max;
    }

    public Cifra<T> crear() {
        return new Cifra.Builder<T>()
                .withValMin(valMin)
                .withValMax(valMax)
                .withMin(min)
                .withMax(max)
                .build();
    }

}
