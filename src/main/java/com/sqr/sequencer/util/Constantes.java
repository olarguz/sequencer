package com.sqr.sequencer.util;

import com.sqr.sequencer.data.Cifra;
import com.sqr.sequencer.mapper.impl.Caracter;
import com.sqr.sequencer.mapper.impl.Entero;

/**
 *
 * @author olmedo.arcila
 */
public class Constantes {

    private Constantes() {
    }
    // Generacion de dato tipo numero.
    static public final Cifra<Entero> N = new FactoryCifra<>(0, 9, new Entero(0), new Entero(9)).crear();
    // Generacion de dato tipo caracter
    static public final Cifra<Caracter> L = new FactoryCifra<>(0, 25, new Caracter('A'), new Caracter('Z')).crear();

}
