package com.sqr.sequencer.util;

import com.sqr.sequencer.data.Cifra;
import com.sqr.sequencer.mapper.Transformable;
import static com.sqr.sequencer.util.Constantes.L;
import static com.sqr.sequencer.util.Constantes.N;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author olmedo.arcila
 */
public class FactoryTipoPlaca {

    public static List<Cifra<? extends Transformable>> generarPatronPlacaVehiculo() {
        return Stream.of(L, L, L, N, N, N).collect(Collectors.toList());
    }

    public static List<Cifra<? extends Transformable>> generarPatronPlacaMoto() {
        return Stream.of(L, L, L, N, N, L).collect(Collectors.toList());
    }
}
