package com.sqr.sequencer.gen;

import com.sqr.sequencer.data.Cifra;
import com.sqr.sequencer.data.Placa;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import com.sqr.sequencer.mapper.Transformable;

/**
 *
 * @author olmedo.arcila
 */
public class Patron {

    private List<Cifra<? extends Transformable>> l;

    public Patron(List<Cifra<? extends Transformable>> l) {
        Collections.reverse(l);
        this.l = l;
    }

    public Placa obtener(final int valor) {
        int valorInterno = valor;
        List<Integer> lx = l.stream().map(e -> e.getValMax() + 1).collect(Collectors.toList());
        List<Integer> lt = new LinkedList<>();
        for (Integer e : lx) {
            int v = valorInterno % e;
            valorInterno = valorInterno / e;
            lt.add(v);
        }
        List<Transformable> resp = new LinkedList<>();
        for (int i = 0; i < lt.size(); i++) {
            Integer vs = lt.get(i);
            Cifra a = l.get(i);
            Transformable r = (Transformable) obtener(a, vs);
            resp.add(0, r);
        }
        String numero = resp.stream().map(e -> e.getValorComun()).collect(Collectors.joining());
        return new Placa(numero, valor);
    }

    public static <T extends Transformable> T obtener(Cifra<T> cifra, int valor) {
        if (cifra.getValMin() <= valor && valor <= cifra.getValMax()) {
            T min = cifra.getMin();
            return (T) min.add(valor);
        } else {
            return null;
        }
    }

}
