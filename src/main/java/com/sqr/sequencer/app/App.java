package com.sqr.sequencer.app;

import com.sqr.sequencer.gen.Patron;
import com.sqr.sequencer.data.Placa;
import com.sqr.sequencer.util.FactoryTipoPlaca;

/**
 *
 * @author olmedo.arcila
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Patron patronVehiculos = new Patron(FactoryTipoPlaca.generarPatronPlacaVehiculo());
        Placa placaVehiculo = patronVehiculos.obtener(0);
        System.out.println(placaVehiculo.getNumero());
        System.out.println(patronVehiculos.obtener(17575999).getNumero());
        System.out.println(patronVehiculos.obtener(3749).getNumero());

        Patron patronMotos = new Patron(FactoryTipoPlaca.generarPatronPlacaMoto());
        Placa placaMoto = patronMotos.obtener(0);
        System.out.println(placaMoto.getNumero());
        System.out.println(patronMotos.obtener(45697599).getNumero());
    }
}
