package com.sqr.sequencer.data;

import com.sqr.sequencer.mapper.Transformable;

/**
 *
 * @author olmedo.arcila
 * @param <T>
 */
public class Cifra<T extends Transformable> {

    private int valMin;
    private int valMax;

    private T min;
    private T max;

    private Cifra() {
    }

    /**
     * Get the value of max
     *
     * @return the value of max
     */
    public T getMax() {
        return max;
    }

    /**
     * Set the value of max
     *
     * @param max new value of max
     */
    public void setMax(T max) {
        this.max = max;
    }

    /**
     * Get the value of min
     *
     * @return the value of min
     */
    public T getMin() {
        return min;
    }

    /**
     * Set the value of min
     *
     * @param min new value of min
     */
    public void setMin(T min) {
        this.min = min;
    }

    /**
     * Get the value of valMax
     *
     * @return the value of valMax
     */
    public int getValMax() {
        return valMax;
    }

    /**
     * Set the value of valMax
     *
     * @param valMax new value of valMax
     */
    public void setValMax(int valMax) {
        this.valMax = valMax;
    }

    /**
     * Get the value of valMin
     *
     * @return the value of valMin
     */
    public int getValMin() {
        return valMin;
    }

    /**
     * Set the value of valMin
     *
     * @param valMin new value of valMin
     */
    public void setValMin(int valMin) {
        this.valMin = valMin;
    }

    public static class Builder<T extends Transformable> {

        private Cifra<T> cifra;

        public Builder() {
            cifra = new Cifra();
        }

        public Builder<T> withValMin(int valMin) {
            cifra.setValMax(valMin);
            return this;
        }

        public Builder<T> withValMax(int valMax) {
            cifra.setValMax(valMax);
            return this;
        }

        public Builder<T> withMin(T min) {
            cifra.setMin(min);
            return this;
        }

        public Builder<T> withMax(T max) {
            cifra.setMax(max);
            return this;
        }

        public Cifra<T> build() {
            return cifra;
        }
    }
}
