package com.sqr.sequencer.data;

/**
 *
 * @author olmedo.arcila
 */
public class Placa implements Sequenciable {

    private final String numero;
    private final int secuencia;

    public Placa(String numero, int secuencia) {
        this.numero = numero;
        this.secuencia = secuencia;
    }

    /**
     * Get the value of secuencia
     *
     * @return the value of secuencia
     */
    public int getSecuencia() {
        return secuencia;
    }

    /**
     * Get the value of numero
     *
     * @return the value of numero
     */
    public String getNumero() {
        return numero;
    }

}
