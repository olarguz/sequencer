package com.sqr.sequencer.mapper.impl;

import com.sqr.sequencer.mapper.Transformable;

/**
 *
 * @author olmedo.arcila
 */
public class Caracter implements Transformable {

    private char valor;

    public Caracter(char valor) {
        this.valor = valor;
    }

    /**
     * Get the value of valor
     *
     * @return the value of valor
     */
    public char getValor() {
        return valor;
    }

    /**
     * Set the value of valor
     *
     * @param valor new value of valor
     */
    public void setValor(char valor) {
        this.valor = valor;
    }

    @Override
    public Transformable add(int valor) {
        return new Caracter((char)((int)this.valor+valor));
    }

    @Override
    public String toString() {
        return "" + valor;
    }

    @Override
    public String getValorComun() {
        return String.valueOf(valor);
    }

}
