package com.sqr.sequencer.mapper.impl;

import com.sqr.sequencer.mapper.Transformable;

/**
 *
 * @author olmedo.arcila
 */
public class Entero implements Transformable {

    private int valor;

    public Entero(int valor) {
        this.valor = valor;
    }

    /**
     * Get the value of valor
     *
     * @return the value of valor
     */
    public int getValor() {
        return valor;
    }

    /**
     * Set the value of valor
     *
     * @param valor new value of valor
     */
    public void setValor(int valor) {
        this.valor = valor;
    }

    @Override
    public Transformable add(int valor) {
        return new Entero(this.valor + valor);
    }

    @Override
    public String toString() {
        return "" + valor;
    }

    @Override
    public String getValorComun() {
        return String.valueOf(valor);
    }

}
