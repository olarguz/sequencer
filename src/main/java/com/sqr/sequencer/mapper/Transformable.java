package com.sqr.sequencer.mapper;

/**
 *
 * @author olmedo.arcila
 */
public interface Transformable {

    /**
     *
     * @param valor
     * @return
     */
    Transformable add(int valor);
    
    /**
     *
     * @return
     */
    String getValorComun ();
}
